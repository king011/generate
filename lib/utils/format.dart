String formatDatetime(DateTime dt) {
  var str = dt.toString();
  final find = str.indexOf(".");
  if (find != -1) {
    str = str.substring(0, find);
  }
  return str;
}
