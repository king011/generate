import 'dart:io';
import 'dart:convert';
import './format.dart';

class Version {
  static const allowed = ["git"];
  static const defaultsTo = "git";
  String datetime;
  String tag;
  String commit;
  Version({this.tag, this.commit, this.datetime})
      : assert(datetime != null),
        assert(tag != null),
        assert(commit != null);
  Version.fromControl(String control) {
    switch (control) {
      case "git":
        _fromGit();
        break;
      default:
        throw UnsupportedError("not support version control :$control");
    }
  }
  _fromGit() {
    var result = Process.runSync("git", ["describe"]);
    tag = result.stdout.toString().trim();
    if (tag.isEmpty) {
      tag = "[unknown tag]";
    }

    result = Process.runSync("git", ["rev-parse", "HEAD"]);
    commit = result.stdout.toString().trim();
    if (commit.isEmpty) {
      commit = "[unknow commit]";
    }

    datetime = formatDatetime(DateTime.now());
  }

  Map<String, dynamic> toJson() => {
        "tag": tag,
        "commit": commit,
        "datetime": datetime,
      };
  @override
  String toString() {
    return JsonEncoder.withIndent(" ").convert(this);
  }
}
