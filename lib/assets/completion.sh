#!/bin/bash
#Program:
#       generate for bash completion
#History:
#       tools auto create at {{environment.now}}
#       tag {{environment.tag}}
#       commit {{environment.commit}}
#       datetime {{environment.datetime}}
#Email:
#       zuiwuchang@gmail.com


function {{tag}}_bash_completion(){
  local opts='-h --help \
    -o --output'

  case ${COMP_WORDS[COMP_CWORD-1]} in
    -o|--output)
      _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
    ;;

    *)
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;
  esac  
}
function {{tag}}_dart_version(){
  local opts='-h --help \
    -o --output --list --mode --template'

  case ${COMP_WORDS[COMP_CWORD-1]} in
    -o|--output)
      _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
    ;;

    --mode)
      opts="create touch update"
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;
    
    --template)
      opts=`generate dart version --list`
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;

    *)
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;
  esac
}
function {{tag}}_dart(){
  if [ 2 == $COMP_CWORD ];then
    local opts='-h --help \
      version'
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
  else
    case ${COMP_WORDS[2]} in

      version)
      {{tag}}_dart_version
      ;;

    esac
  fi
}

function {{tag}}_go_version(){
  local opts='-h --help \
    -o --output --list --mode --template'

  case ${COMP_WORDS[COMP_CWORD-1]} in
    -o|--output)
      _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
    ;;

    --mode)
      opts="create touch update"
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;
    
    --template)
      opts=`generate go version --list`
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;

    *)
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;
  esac
}

function {{tag}}_go_init(){
  local opts='-h --help \
    -o --output --list --mode --template --package-name --project-name'

  case ${COMP_WORDS[COMP_CWORD-1]} in
    -o|--output)
      _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
    ;;


    --mode)
      opts="create touch update"
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;
    
    --template)
      opts=`generate go init --list`
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;

    *)
      COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    ;;
  esac
}

function {{tag}}_go(){
  if [ 2 == $COMP_CWORD ];then
    local opts='-h --help \
      init version'
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
  else
    case ${COMP_WORDS[2]} in

      init)
        {{tag}}_go_init
      ;;

      version)
        {{tag}}_go_version
      ;;
    esac
  fi
}
function {{tag}}(){
	local cur=${COMP_WORDS[COMP_CWORD]}
	if [ 1 == $COMP_CWORD ];then
		local opts='-h --help -v --version \
			bash-completion \
			go dart'
		COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	else
		case ${COMP_WORDS[1]} in

			bash-completion)
				{{tag}}_bash_completion
			;;

			dart)
				{{tag}}_dart
			;;

			go)
				{{tag}}_go
			;;
		esac
	fi
}

complete -F {{tag}} generate
