import './cmd/root.dart' as root;
import './cmd/dart.dart' as dart;
import './cmd/go.dart' as go;
import './cmd/completion.dart' as completion;

void runMain(List<String> arguments) {
  root.init()
    ..addCommand([
      dart.init(),
      go.init(),
      completion.init(),
    ])
    ..parseAndExecute(arguments);
}
