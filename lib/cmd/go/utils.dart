String formatPackage(String name) {
  if (name == null) {
    throw UnsupportedError("package not support : null");
  }
  var str = name.trim();
  if (str.startsWith("/")) {
    str = str.substring(1);
  }
  if (str.endsWith("/")) {
    str = str.substring(
      0,
      str.length - 1,
    );
  }
  str = str.trim();
  if (str.isEmpty) {
    throw UnsupportedError("package not support : $name");
  }
  if (str.contains(r"//")) {
    throw UnsupportedError("package not support : $name");
  }
  if (!RegExp(r"^[0-9a-zA-Z][0-9a-zA-Z\_\/]*$").hasMatch(str)) {
    throw UnsupportedError("package not support : $name");
  }

  return str;
}

String packageToProject(String name) {
  var strs = name.split("/");
  return strs[strs.length - 1];
}

String formatProject(String name) {
  if (name == null) {
    throw UnsupportedError("project not support : null");
  }
  var str = name.trim();
  if (str.startsWith("/")) {
    str = str.substring(1);
  }
  if (str.endsWith("/")) {
    str = str.substring(
      0,
      str.length - 1,
    );
  }
  str = str.trim();
  if (str.isEmpty) {
    throw UnsupportedError("project not support : $name");
  }
  if (!RegExp(r"^[0-9a-zA-Z][0-9a-zA-Z\_]*$").hasMatch(str)) {
    throw UnsupportedError("project not support : $name");
  }
  return str;
}
