import 'dart:io';
import 'package:wrapper_args/wrapper_args.dart';
import '../version.dart';

Command init() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addFlag(
      "version",
      abbr: "v",
      negatable: false,
      help: "Print the version",
    );
  return Command(
    help: "Usage: generate [<subcommand>] [<options>]",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      } else if (results["version"]) {
        if (Version.tag.isNotEmpty) {
          print(Version.tag);
        }
        print(Version.commit);
        print(Version.datetime);
      } else {
        print(command.usage);
        exitCode = 1;
      }
    },
  );
}
