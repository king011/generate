import 'dart:io';
import 'package:wrapper_args/wrapper_args.dart';
import '../utils/version.dart';
import '../template/manager.dart';
import '../context/context.dart';
import './go/export.dart';

Command init() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    );
  return Command(
    name: "go",
    help: "generate go code",
    longHelp: "Usage: generate go [<subcommand>] [<options>]",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      }
    },
  )..addCommand(
      [
        _init(),
        _version(),
      ],
    );
}

Command _version() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addFlag(
      "list",
      negatable: false,
      help: "list all supported template",
    )
    ..addOption(
      "control",
      defaultsTo: Version.defaultsTo,
      allowed: Version.allowed,
      help: "version control",
    )
    ..addOption(
      "mode",
      defaultsTo: GenerateMode.touch.toString(),
      allowed: GenerateMode.allowed,
      allowedHelp: GenerateMode.allowedHelp,
      help: "write mode",
    )
    ..addOption(
      "template",
      defaultsTo: "version",
      help: "template name",
    )
    ..addOption(
      "output",
      abbr: "o",
      defaultsTo: "version/version.go",
    );
  final tag = "go/version";
  return Command(
    name: "version",
    help: "generate go version code from source",
    longHelp: "Usage: generate go version [<options>]",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print("${command.usage}");
        return 0;
      }
      if (results["list"]) {
        print(Manager.instance.list(
          tag,
          builtin: ["version"],
        ).join(" "));
        return 0;
      }
      final mode = GenerateMode.fromString(results["mode"]);
      final output = results["output"];
      () async {
        // check root
        final stat = await FileStat.stat("go.mod");
        if (stat.type == FileSystemEntityType.notFound) {
          throw StateError("go.mod not found");
        }
        // get version
        final version = Version.fromControl(results["control"]);
        print(version.toString());

        // get template
        final manager = Manager.instance;
        final info = manager.find(tag, results["template"]);
        if (info == null) {
          await manager.readerSourceFile(
            tag: "go",
            item: Info(
              file: "version",
              directory: false,
              filemode: ModeFile,
            ),
            mode: mode,
            output: output,
            values: {
              "version": version,
              "environment": Environment(),
            },
          );
        } else {
          await manager.reader(
            info: info,
            mode: mode,
            output: output,
            values: {
              "version": version,
              "environment": Environment(),
            },
          );
        }
        return 0;
      }();
    },
  );
}

Command _init() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addFlag(
      "list",
      negatable: false,
      help: "list all supported template",
    )
    ..addOption(
      "control",
      defaultsTo: Version.defaultsTo,
      allowed: Version.allowed,
      help: "version control",
    )
    ..addOption(
      "mode",
      defaultsTo: GenerateMode.create.toString(),
      allowed: GenerateMode.allowed,
      allowedHelp: GenerateMode.allowedHelp,
      help: "write mode",
    )
    ..addOption(
      "template",
      defaultsTo: "console",
      allowedHelp: {
        "console": "A command-line application sample",
      },
      help: "project template",
    )
    ..addOption(
      "package-name",
      help: "package name",
    )
    ..addOption(
      "project-name",
      help: "package name",
    );
  final tag = "go/init";
  return Command(
    name: "init",
    help: "init go project at this directory",
    longHelp: "Usage: generate go init [<options>]",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print("${command.usage}");
        return 0;
      }
      if (results["list"]) {
        print(Manager.instance.list(
          tag,
          builtin: ["console"],
        ).join(" "));
        return 0;
      }
      // context
      String packageName = results["package-name"] ?? "";
      if (packageName.isEmpty) {
        throw ArgumentError("--package-name not specified");
      }
      packageName = formatPackage(packageName);
      String projectName =
          results["project-name"] ?? packageToProject(packageName);
      if (projectName.isEmpty) {
        throw ArgumentError("--project-name not specified");
      }
      projectName = formatProject(projectName);
      final version = Version.fromControl(Version.defaultsTo);
      () async {
        final mode = GenerateMode.fromString(results["mode"]);
        final output = ".";
        // get template
        final manager = Manager.instance;
        final template = results["template"];
        final info = manager.find(tag, template);
        final values = {
          "package": packageName,
          "project": projectName,
          "version": version,
          "environment": Environment(),
          "grpc": packageName.replaceAll("/", "_"),
        };
        if (info == null) {
          switch (template) {
            case "console":
              await initReaderConsole(manager, mode, output, values);
              break;
            default:
              throw ArgumentError.value("not support template : ${template}");
          }
        } else {
          await manager.reader(
            info: info,
            mode: mode,
            output: output,
            values: values,
          );
        }
      }();
    },
  );
}

initReaderConsole(
  Manager manager,
  GenerateMode mode,
  String output,
  dynamic values,
) async {
  await manager.readerSource(
    tag: "go/init/console",
    items: [
      // 通用 資源
      Info(
        file: ".vscode/launch.json",
        resource: "../../vscode/launch.json",
      ),
      Info(
        file: ".vscode/settings.json",
        resource: "../../vscode/settings.json",
      ),
      Info(
        file: ".vscode/tasks.json",
        resource: "../../vscode/tasks.json",
      ),
      Info(
        file: "version/version.go",
        resource: "../../version",
      ),
      Info(
        file: ".gitignore",
        resource: "../../gitignore",
      ),
      Info(
        file: "go.mod",
        resource: "../../go.mod",
      ),
      Info(
        file: "build.sh",
        resource: "../../build",
        filemode: ModeExecutable,
      ),
      // 項目獨有資源
      Info(
        file: "main.go",
        resource: "main",
      ),
      Info(
        file: "cmd/init.go",
        resource: "cmd",
      ),
    ],
    mode: mode,
    output: output,
    values: values,
  );
}
