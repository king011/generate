import 'dart:io';
import 'package:wrapper_args/wrapper_args.dart';
import '../context/context.dart';
import '../utils/version.dart';
import '../template/manager.dart';

Command init() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    );
  return Command(
    name: "dart",
    help: "generate dart code",
    longHelp: "Usage: generate dart [<subcommand>] [<options>]",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      }
    },
  )..addCommand(
      [
        _version(),
      ],
    );
}

Command _version() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addFlag(
      "list",
      negatable: false,
      help: "list all supported template",
    )
    ..addOption(
      "control",
      defaultsTo: Version.defaultsTo,
      allowed: Version.allowed,
      help: "version control",
    )
    ..addOption(
      "mode",
      defaultsTo: GenerateMode.touch.toString(),
      allowed: GenerateMode.allowed,
      allowedHelp: GenerateMode.allowedHelp,
      help: "write mode",
    )
    ..addOption(
      "template",
      defaultsTo: "version",
      help: "template name",
    )
    ..addOption(
      "output",
      abbr: "o",
      defaultsTo: "lib/version.dart",
    );
  final tag = "dart/version";
  return Command(
    name: "version",
    help: "generate dart version code from source",
    longHelp: "Usage: generate dart version [<options>]",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print("${command.usage}");
        return 0;
      }
      if (results["list"]) {
        print(Manager.instance.list(
          tag,
          builtin: ["version"],
        ).join(" "));
        return 0;
      }
      final mode = GenerateMode.fromString(results["mode"]);
      final output = results["output"];
      () async {
        // check root
        final stat = await FileStat.stat("pubspec.yaml");
        if (stat.type == FileSystemEntityType.notFound) {
          throw StateError("pubspec.yaml not found");
        }
        // get version
        final version = Version.fromControl(results["control"]);
        print(version.toString());

        // get template
        final manager = Manager.instance;
        final info = manager.find(tag, results["template"]);
        if (info == null) {
          await manager.readerSourceFile(
            tag: "dart",
            item: Info(
              file: "version",
              directory: false,
              filemode: ModeFile,
            ),
            mode: mode,
            output: output,
            values: {
              "version": version,
              "environment": Environment(),
            },
          );
        } else {
          await manager.reader(
            info: info,
            mode: mode,
            output: output,
            values: {
              "version": version,
              "environment": Environment(),
            },
          );
        }
        return 0;
      }();
    },
  );
}
