import 'dart:io';
import 'dart:async';
import 'dart:convert' show utf8;
import 'package:wrapper_args/wrapper_args.dart';
import 'package:resource/resource.dart' show Resource;
import 'package:mustache/mustache.dart';
import '../context/context.dart';

Command init() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addOption(
      "output",
      abbr: "o",
    );
  return Command(
    name: "bash-completion",
    help: "generate bash-completion for bash shell",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      } else {
        final controller = StreamController<int>();
        controller.stream.listen((v) {
          exitCode = v ?? 0;
        });
        () async {
          final output = results["output"];
          IOSink writer;
          if (output == null) {
            writer = stdout;
          } else {
            var file = File(output);
            file = await file.create(recursive: false);
            writer = file.openWrite();
          }
          final resource = Resource("package:generate/assets/completion.sh");
          final source = await resource.readAsString(encoding: utf8);
          final template = Template(
            source,
            htmlEscapeValues: false,
          );
          writer.write(template.renderString({
            "tag": "__king011_generate",
            "environment": Environment(),
          }));
          if (output != null) {
            await writer.close();
          }
        }();
      }
    },
  );
}
