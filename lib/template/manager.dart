import 'dart:io';
import 'dart:convert' show utf8, jsonDecode;
import 'package:resource/resource.dart' show Resource;
import 'package:path/path.dart';
import 'package:mustache/mustache.dart';

// 0775
const ModeDirectory = (7 << 6) | (7 << 3) | 5;
// 0664
const ModeFile = (6 << 6) | (6 << 3) | 4;
// 0775
const ModeExecutable = (7 << 6) | (7 << 3) | (5);

class GenerateMode {
  final int _type;
  static const touch = const GenerateMode._internal(0);
  static const create = const GenerateMode._internal(1);
  static const update = const GenerateMode._internal(2);
  static const allowed = ['touch', 'create', 'update'];
  static const allowedHelp = {
    "touch": "always generate code",
    "create": "generate code only if file not exists",
    "update": "generate code only if file exists",
  };
  const GenerateMode._internal(this._type);
  static const _typeList = const [
    GenerateMode.touch,
    GenerateMode.create,
    GenerateMode.update,
  ];
  String toString() => ['touch', 'create', 'update'][_type];
  static GenerateMode fromString(String str) {
    for (var i = 0; i < GenerateMode.allowed.length; i++) {
      if (str == GenerateMode.allowed[i]) {
        return GenerateMode._typeList[i];
      }
    }
    throw ArgumentError("not support $str");
  }
}

class Manager {
  static Manager _instance;
  static Manager get instance {
    if (_instance == null) {
      _instance = Manager._();
    }
    return _instance;
  }

  final _path = List<String>();
  Manager._() {
    var str = Platform.environment["DART_GENERATE_TEMPLATE"];
    if (str == null) {
      return;
    }
    List<String> strs;
    if (Platform.isWindows) {
      strs = str.split(";");
    } else {
      strs = str.split(":");
    }
    for (var i = 0; i < strs.length; i++) {
      if (isAbsolute(strs[i])) {
        _path.add(normalize(strs[i]));
      } else {
        _path.add(normalize(absolute(strs[i])));
      }
    }
  }

  /// find template
  Info find(String tag, String name) {
    for (var dir in _path) {
      dir = join(dir, tag, name);
      final file = File(dir);
      final stat = file.statSync();
      if (stat.type == FileSystemEntityType.notFound) {
        continue;
      } else if (stat.type == FileSystemEntityType.file) {
        return Info(directory: false, file: dir);
      } else if (stat.type == FileSystemEntityType.directory) {
        return Info(directory: true, file: dir);
      }
    }
    return null;
  }

  /// list all template
  List<String> list(
    String tag, {
    List<String> builtin,
  }) {
    final keys = Map<String, bool>();
    final arrs = List<String>();

    for (var dir in _path) {
      final directory = Directory(join(dir, tag));
      try {
        final fs = directory.listSync();
        if (fs == null) {
          continue;
        }
        for (var f in fs) {
          final name = basename(f.path);
          if (keys.containsKey(name)) {
            continue;
          }
          keys[name] = true;
          arrs.add(name);
        }
      } catch (_) {}
    }

    if (builtin != null) {
      for (var name in builtin) {
        if (keys.containsKey(name)) {
          continue;
        }
        keys[name] = true;
        arrs.add(name);
      }
    }
    return arrs;
  }

  reader({
    Info info,
    GenerateMode mode,
    String output,
    dynamic values,
  }) async {
    if (info.directory) {
      await _readerDirectory(
        filename: info.file,
        mode: mode,
        output: output,
        values: values,
      );
    } else {
      await _readerFile(
        filename: info.file,
        mode: mode,
        output: output,
        values: values,
      );
    }
  }

  readerSourceFile({
    String tag,
    Info item,
    GenerateMode mode,
    String output,
    dynamic values,
  }) async {
    var file = File(output);
    if (mode != GenerateMode.touch) {
      final stat = await file.stat();
      if (stat.type == FileSystemEntityType.notFound) {
        if (mode == GenerateMode.update) {
          return;
        }
      } else {
        if (mode != GenerateMode.update) {
          return;
        }
      }
    }
    final src = "package:generate/assets/$tag/${item.resource}";
    final resource = Resource(src);
    final source = await resource.readAsString(encoding: utf8);
    final template = Template(
      source,
      htmlEscapeValues: false,
    );
    final contents = template.renderString(values);
    file = await file.create(recursive: true);
    await file.writeAsString(contents);
    await _syncmode(output, filemode: item.filemode);
  }

  readerSource({
    String tag,
    List<Info> items,
    GenerateMode mode,
    String output,
    dynamic values,
  }) async {
    if (items == null) {
      return;
    }
    for (var i = 0; i < items.length; i++) {
      final info = items[i];
      if (info.directory) {
        final target = "$output/${info.file}";
        await _mkdir(
          mode: mode,
          output: target,
          filemode: info.filemode,
        );
      } else {
        await readerSourceFile(
          tag: tag,
          item: info,
          mode: mode,
          output: join(output, info.file),
          values: values,
        );
      }
    }
    return;
  }

  _readerDirectory({
    String filename,
    GenerateMode mode,
    String output,
    dynamic values,
  }) async {
    final start = filename.length + 1;

    NamesMapping mapping;
    try {
      var str = await File(join(filename, "__rename.json"))
          .readAsString(encoding: utf8);
      final template = Template(str, htmlEscapeValues: false);
      str = template.renderString(values);
      mapping = NamesMapping.fromJson(jsonDecode(str));
    } on FileSystemException {}
    await for (var item in Directory(filename).list(recursive: true)) {
      final path = item.path;
      var name = path.substring(start);
      if (name == "__rename.json") {
        continue;
      }
      if (mapping != null) {
        name = mapping.rename(name);
      }

      final target = join(output, name);
      var stat = await item.stat();
      if (stat.type == FileSystemEntityType.file) {
        await _readerFile(
          filename: item.path,
          mode: mode,
          output: target,
          values: values,
        );
      } else if (stat.type == FileSystemEntityType.directory) {
        await _mkdir(
          mode: mode,
          output: target,
          filemode: stat.mode,
        );
      }
    }
  }

  _mkdir({
    GenerateMode mode,
    String output,
    int filemode,
  }) async {
    var file = Directory(output);
    if (mode != GenerateMode.touch) {
      final stat = await file.stat();
      if (stat.type == FileSystemEntityType.notFound) {
        if (mode == GenerateMode.update) {
          return;
        }
      } else {
        if (mode != GenerateMode.update) {
          return;
        }
      }
    }
    await file.create(recursive: true);
    _syncmode(output, filemode: filemode);
  }

  _readerFile({
    String filename,
    GenerateMode mode,
    String output,
    dynamic values,
  }) async {
    String source = await File(filename).readAsString(encoding: utf8);
    var file = File(output);
    if (mode != GenerateMode.touch) {
      final stat = await file.stat();
      if (stat.type == FileSystemEntityType.notFound) {
        if (mode == GenerateMode.update) {
          return;
        }
      } else {
        if (mode != GenerateMode.update) {
          return;
        }
      }
    }
    final template = Template(
      source,
      htmlEscapeValues: false,
    );
    final contents = template.renderString(values);
    file = await file.create(recursive: true);
    await file.writeAsString(contents);
    await _syncmode(output, filename: filename);
  }

  _syncmode(String output, {String filename, int filemode}) async {
    if (filemode == null) {
      final src = await File(filename).stat();
      filemode = src.mode;
    }
    filemode &= 0x1FF;
    final dst = await File(output).stat();
    if (filemode != dst.mode & 0x1FF) {
      final str = filemode.toRadixString(8);
      try {
        Process.runSync("chmod", [str, "$output"]);
      } catch (_) {}
    }
  }
}

class Info {
  bool directory;
  String _file;
  String _resource;
  int filemode;
  Info({
    this.directory = false,
    String file,
    String resource,
    this.filemode = ModeFile,
  })  : assert(directory != null),
        assert(file != null),
        assert(filemode != null),
        _file = file,
        _resource = resource;
  String get file => _file;
  String get resource => _resource ?? _file;
}

class NameMapping {
  RegExp from;
  String replace;
  NameMapping.fromJson(Map<String, dynamic> json)
      : this.from = RegExp(json["from"]),
        this.replace = json["replace"];
}

class NamesMapping {
  List<NameMapping> mapping;
  NamesMapping.fromJson(List<dynamic> json) {
    if (json == null || json.isEmpty) {
      return;
    }
    mapping = List<NameMapping>(json.length);
    for (var i = 0; i < json.length; i++) {
      mapping[i] = NameMapping.fromJson(json[i]);
    }
  }
  String rename(String name) {
    if (mapping == null || mapping.isEmpty) {
      return name;
    }
    for (var i = 0; i < mapping.length; i++) {
      name = name.replaceAll(mapping[i].from, mapping[i].replace);
    }
    return name;
  }
}
