import '../version.dart';
import '../utils/format.dart';

class Environment {
  const Environment();
  // generate info
  String get tag => Version.tag;
  String get commit => Version.commit;
  String get datetime => Version.datetime;
  // now string
  String get now => formatDatetime(DateTime.now());
}
