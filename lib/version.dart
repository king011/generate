/// version.dart create by generate at 2019-04-04 12:41:08
/// tag : v0.2.0
/// commit : 967d115575cced15e28b70d0886f2f266a6c7fc2
/// datetime : 2019-04-03 15:30:45
class Version {
  Version._();
  static String get tag => 'v0.2.0-3-g1ccc454';
  static String get commit => '1ccc4547f399a2f2ac005b661fe730f71dd4671e';
  static String get datetime => '2019-04-04 12:41:07';
}
