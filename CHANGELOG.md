## 0.2.1+3
- support rename template
_ fix some bug

## 0.2.0
- support user template

## 0.1.0+3
- update bash-completion
- update go init create build-go.sh
- update go init create grpc code

## 0.1.0
- generate golang code for console,version

## 0.0.2

- Initial version, created by Stagehand
