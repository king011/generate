import 'package:generate/generate.dart' as generate;

main(List<String> arguments) {
  generate.runMain(arguments);
}
