# generate

generate 是一個 命令行工具 用來自動 創建一些 程式語言的 代碼

# DART_GENERATE_TEMPLATE

generate 創建的 代碼 內置 了 一些模板 

同時 generate 允許 用戶創建一些 自定義的 代碼模板 generate 或從 環境變量 DART_GENERATE_TEMPLATE 中 查找 代碼模板

代碼模板 可以是 一個 檔案夾/檔案 以這個 檔案夾/檔案 作爲 模板名

# 模板位置

自定義模板 應該放置在 DART_GENERATE_TEMPLATE 中和 當前 命令 相同的 子目錄中 

比如 `generate dart version` 指令 會 查找 `$DART_GENERATE_TEMPLATE/dart/version` 下的 所有模板


# 公共模板 
[https://gitlab.com/king011/generate_template.git](https://gitlab.com/king011/generate_template.git)